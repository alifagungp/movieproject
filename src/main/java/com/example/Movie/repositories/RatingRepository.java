package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Movie.models.Rating;
import com.example.Movie.models.RatingId;

@Repository
public interface RatingRepository extends JpaRepository<Rating, RatingId>{

}
