package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Movie.models.MovieCast;

@Repository
public interface MovieCastRepository extends JpaRepository<MovieCast, Object>{

}
