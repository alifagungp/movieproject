package com.example.Movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Movie.models.Director;

@Repository
public interface DirectorRepository extends JpaRepository<Director, Long> {

}
