package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.GenreDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Genre;
import com.example.Movie.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	
	@Autowired
	GenreRepository genreRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/genre/readAll")
	public HashMap<String, Object> readAllGenre(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();

		for(Genre genre : listGenreEntity) {
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			
			listGenreDTO.add(genreDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Genre Data Success");
		result.put("Data", listGenreDTO);
		
		return result;
	}
	
	@PostMapping("/genre/create")
	public HashMap<String, Object> createGenreDTO(@Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genreEntity = modelMapper.map(genreDTO, Genre.class);		
		genreRepository.save(genreEntity);
		
		genreDTO = modelMapper.map(genreEntity, GenreDTO.class);
		genreDTO.setId(genreEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	@PutMapping("/genre/update/{id}")
	public HashMap<String, Object> updateGenreDTO(@PathVariable(value = "id")Long Id, @Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Genre genreEntity = genreRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Genre", "id" , Id));
		genreEntity = modelMapper.map(genreDTO, Genre.class);
		genreEntity.setId(Id);		
		genreRepository.save(genreEntity);
		genreDTO = modelMapper.map(genreEntity, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Selected Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	@DeleteMapping("/genre/delete/{id}")
	public HashMap<String, Object> deleteGenreDTO(@PathVariable(value = "id")Long Id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Genre genreEntity = genreRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", Id));
		GenreDTO genreDTO = modelMapper.map(genreEntity, GenreDTO.class);
		genreRepository.delete(genreEntity);
	
		result.put("Status", 200);
		result.put("Message", "Delete Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
}
