package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.MovieCastDTO;
import com.example.Movie.models.MovieCast;
import com.example.Movie.models.MovieCastId;
import com.example.Movie.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	@Autowired
	MovieCastRepository movieCastRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/movie_cast/create")
	public HashMap<String, Object> createMovieCastDTO(@Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		boolean isValid = true;
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		for(MovieCast movieCast : listMovieCastEntity) {
			if (movieCast.getId().getActId() == movieCastDTO.getId().getActId() && movieCast.getId().getMovId() == movieCastDTO.getId().getMovId()) {
				isValid = false;
				break;
			}
		}
		
		if (isValid == true) {
			MovieCast movieCastEntity = modelMapper.map(movieCastDTO, MovieCast.class);		
			movieCastRepository.save(movieCastEntity);
			
			movieCastDTO = modelMapper.map(movieCastEntity, MovieCastDTO.class);
			movieCastDTO.setId(movieCastEntity.getId());
			
			result.put("Status", 200);
			result.put("Message", "Create New Movie Cast Success");
			result.put("Data", movieCastDTO);
		}else {
			result.put("Status", 300);
			result.put("Message", "Create New Movie Cast Failed, actId & movId is exist");
		}

		return result;
	}
	
	@GetMapping("/movie_cast/readAll")
	public HashMap<String, Object> readAllMovieCast(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		ArrayList<MovieCastDTO> listMovieCastDTO = new ArrayList<MovieCastDTO>();

		for(MovieCast movieCast : listMovieCastEntity) {
			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			
			listMovieCastDTO.add(movieCastDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All MovieCast Data Success");
		result.put("Data", listMovieCastDTO);
		
		return result;
	}
	
	@PutMapping("/movie_cast/update/{actId}/{movId}")
	public HashMap<String, Object> updateDataDTO (@PathVariable (value = "actId") Long actId,@PathVariable (value = "movId") Long movId,  @Valid @RequestBody  MovieCastDTO movieCastDTO) {
		HashMap<String, Object> result = new HashMap<String, Object>();
	
		boolean isValid = true;
		MovieCastId movieCastId = new MovieCastId();
		movieCastId.setActId(actId);
		movieCastId.setMovId(movId);
	
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>)movieCastRepository.findAll();
		for (MovieCast movieCast : listMovieCastEntity) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
				movieCast.setRole(movieCastDTO.getRole());
				movieCastRepository.save(movieCast);
				movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
				isValid = true;
			}
			else {
				isValid = false;
			}
		}
		
		if (isValid) {
			result.put("Status", 200);
			result.put("Message", "Update Movie Cast Succes");
			result.put("Data", movieCastDTO);
		}
		else {
			result.put("Status", 400);
			result.put("Message", "Data Not Found");
		}
	
		return result;
	}
	
	@DeleteMapping("/movie_cast/delete/{actId}/{movId}")
	public HashMap<String, Object> updateDataDTO (@PathVariable (value = "actId") Long actId,@PathVariable (value = "movId") Long movId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
	
		boolean isValid = true;
		MovieCastId movieCastId = new MovieCastId();
		MovieCastDTO movieCastDTO = new MovieCastDTO();
		movieCastId.setActId(actId);
		movieCastId.setMovId(movId);
	
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>)movieCastRepository.findAll();
		for (MovieCast movieCast : listMovieCastEntity) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
				movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
				movieCastRepository.delete(movieCast);
				isValid = true;
			}
			else {
				isValid = false;
			}
			
		}
		
		if (isValid) {
			result.put("Status", 200);
			result.put("Message", "Delete Movie Cast Succes");
			result.put("Data", movieCastDTO);
		}
		else {
			result.put("Status", 400);
			result.put("Message", "Data Not Found");
		}
	
		return result;
	}
}
