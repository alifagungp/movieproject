package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.RatingDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Rating;
import com.example.Movie.models.RatingId;
import com.example.Movie.repositories.RatingRepository;

@RestController
@RequestMapping("api")
public class RatingController {
	@Autowired
	RatingRepository ratingRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/rating/readAll")
	public HashMap<String, Object> readAllMovie(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Rating> listRatingEntity = (ArrayList<Rating>) ratingRepository.findAll();
		ArrayList<RatingDTO> listRatingDTO = new ArrayList<RatingDTO>();

		for(Rating rating : listRatingEntity) {
			RatingDTO ratingDTO = modelMapper.map(rating, RatingDTO.class);
			
			listRatingDTO.add(ratingDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Rating Data Success");
		result.put("Data", listRatingDTO);
		
		return result;
	}
	
	@PostMapping("/rating/create")
	public HashMap<String, Object> createRatingDTO(@Valid @RequestBody RatingDTO ratingDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Rating ratingEntity = modelMapper.map(ratingDTO, Rating.class);		
		ratingRepository.save(ratingEntity);
		
		ratingDTO = modelMapper.map(ratingEntity, RatingDTO.class);
		ratingDTO.setId(ratingEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Rating Success");
		result.put("Data", ratingDTO);
		
		return result;
	}
	
//	@PutMapping("/rating/update/{movId}{revId}")
//	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "movId")Long movId, @PathVariable(value = "movId")Long revId, @Valid @RequestBody RatingDTO ratingDTO){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		Rating ratingEntity = ratingRepository.findById(Id)
//				.orElseThrow(() -> new ResourceNotFoundException("Rating", "id" , Id));
//		ratingEntity = modelMapper.map(ratingDTO, Rating.class);
//		ratingEntity.setId(Id);		
//		ratingRepository.save(ratingEntity);
//		ratingDTO = modelMapper.map(ratingEntity, RatingDTO.class);
//		
//		result.put("Status", 200);
//		result.put("Message", "Update Selected Rating Success");
//		result.put("Data", ratingDTO);
//		
//		return result;
//	}
//	
//	@DeleteMapping("/rating/delete/{id}")
//	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id")Long Id){
//		HashMap<String, Object> result = new HashMap<String, Object>();
//		Rating ratingEntity = ratingRepository.findById(Id)
//				.orElseThrow(() -> new ResourceNotFoundException("Rating", "id", Id));
//		RatingDTO ratingDTO = modelMapper.map(ratingEntity, RatingDTO.class);
//		ratingRepository.delete(ratingEntity);
//	
//		result.put("Status", 200);
//		result.put("Message", "Delete Rating Success");
//		result.put("Data", ratingDTO);
//		
//		return result;
//	}
}
