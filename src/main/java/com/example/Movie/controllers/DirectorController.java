package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.DirectorDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Director;
import com.example.Movie.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	
	@Autowired
	DirectorRepository directorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/director/readAll")
	public HashMap<String, Object> readAllDirector(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();

		for(Director director : listDirectorEntity) {
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			
			listDirectorDTO.add(directorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Director Data Success");
		result.put("Data", listDirectorDTO);
		
		return result;
	}
	
	@PostMapping("/director/create")
	public HashMap<String, Object> createDirectorDTO(@Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director directorEntity = modelMapper.map(directorDTO, Director.class);		
		directorRepository.save(directorEntity);
		
		directorDTO = modelMapper.map(directorEntity, DirectorDTO.class);
		directorDTO.setId(directorEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	@PutMapping("/director/update/{id}")
	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "id")Long Id, @Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Director directorEntity = directorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id" , Id));
		directorEntity = modelMapper.map(directorDTO, Director.class);
		directorEntity.setId(Id);		
		directorRepository.save(directorEntity);
		directorDTO = modelMapper.map(directorEntity, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Selected Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	@DeleteMapping("/director/delete/{id}")
	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id")Long Id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Director directorEntity = directorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Director", "id", Id));
		DirectorDTO directorDTO = modelMapper.map(directorEntity, DirectorDTO.class);
		directorRepository.delete(directorEntity);
	
		result.put("Status", 200);
		result.put("Message", "Delete Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
}
