package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.Movie.dto.ActorDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Actor;
import com.example.Movie.repositories.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	@Autowired
	ActorRepository actorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/actor/readAll")
	public HashMap<String, Object> readAllActor(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();

		for(Actor actor : listActorEntity) {
			ActorDTO actorDTO = modelMapper.map(actor, ActorDTO.class);
			
			listActorDTO.add(actorDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Actor Data Success");
		result.put("Data", listActorDTO);
		
		return result;
	}
	
	@PostMapping("/actor/create")
	public HashMap<String, Object> createActorDTO(@Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actorEntity = modelMapper.map(actorDTO, Actor.class);		
		actorRepository.save(actorEntity);
		
		actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		actorDTO.setId(actorEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	@PutMapping("/actor/update/{id}")
	public HashMap<String, Object> updateActorDTO(@PathVariable(value = "id")Long Id, @Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Actor actorEntity = actorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id" , Id));
		actorEntity = modelMapper.map(actorDTO, Actor.class);
		actorEntity.setId(Id);		
		actorRepository.save(actorEntity);
		actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Selected Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
	
	@DeleteMapping("/actor/delete/{id}")
	public HashMap<String, Object> deleteActorDTO(@PathVariable(value = "id")Long Id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Actor actorEntity = actorRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Actor", "id", Id));
		ActorDTO actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		actorRepository.delete(actorEntity);
	
		result.put("Status", 200);
		result.put("Message", "Delete Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
}
