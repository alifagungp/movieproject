package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.MovieDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Movie;
import com.example.Movie.repositories.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {

	@Autowired
	MovieRepository movieRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/movie/readAll")
	public HashMap<String, Object> readAllMovie(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		ArrayList<MovieDTO> listMovieDTO = new ArrayList<MovieDTO>();

		for(Movie movie : listMovieEntity) {
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			
			listMovieDTO.add(movieDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Movie Data Success");
		result.put("Data", listMovieDTO);
		
		return result;
	}
	
	@PostMapping("/movie/create")
	public HashMap<String, Object> createMovieDTO(@Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movieEntity = modelMapper.map(movieDTO, Movie.class);		
		movieRepository.save(movieEntity);
		
		movieDTO = modelMapper.map(movieEntity, MovieDTO.class);
		movieDTO.setId(movieEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	@PutMapping("/movie/update/{id}")
	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "id")Long Id, @Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Movie movieEntity = movieRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id" , Id));
		movieEntity = modelMapper.map(movieDTO, Movie.class);
		movieEntity.setId(Id);		
		movieRepository.save(movieEntity);
		movieDTO = modelMapper.map(movieEntity, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Selected Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	@DeleteMapping("/movie/delete/{id}")
	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id")Long Id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Movie movieEntity = movieRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Movie", "id", Id));
		MovieDTO movieDTO = modelMapper.map(movieEntity, MovieDTO.class);
		movieRepository.delete(movieEntity);
	
		result.put("Status", 200);
		result.put("Message", "Delete Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
}
