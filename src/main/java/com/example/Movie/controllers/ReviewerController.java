package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.dto.ReviewerDTO;
import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Reviewer;
import com.example.Movie.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api")
public class ReviewerController {
	@Autowired
	ReviewerRepository reviewerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@GetMapping("/reviewer/readAll")
	public HashMap<String, Object> readAllMovie(){
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<Reviewer> listReviewerEntity = (ArrayList<Reviewer>) reviewerRepository.findAll();
		ArrayList<ReviewerDTO> listReviewerDTO = new ArrayList<ReviewerDTO>();

		for(Reviewer reviewer : listReviewerEntity) {
			ReviewerDTO reviewerDTO = modelMapper.map(reviewer, ReviewerDTO.class);
			
			listReviewerDTO.add(reviewerDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read All Reviewer Data Success");
		result.put("Data", listReviewerDTO);
		
		return result;
	}
	
	@PostMapping("/reviewer/create")
	public HashMap<String, Object> createReviewerDTO(@Valid @RequestBody ReviewerDTO reviewerDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = modelMapper.map(reviewerDTO, Reviewer.class);		
		reviewerRepository.save(reviewerEntity);
		
		reviewerDTO = modelMapper.map(reviewerEntity, ReviewerDTO.class);
		reviewerDTO.setId(reviewerEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Reviewer Success");
		result.put("Data", reviewerDTO);
		
		return result;
	}
	
	@PutMapping("/reviewer/update/{id}")
	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "id")Long Id, @Valid @RequestBody ReviewerDTO reviewerDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = reviewerRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id" , Id));
		reviewerEntity = modelMapper.map(reviewerDTO, Reviewer.class);
		reviewerEntity.setId(Id);		
		reviewerRepository.save(reviewerEntity);
		reviewerDTO = modelMapper.map(reviewerEntity, ReviewerDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Selected Reviewer Success");
		result.put("Data", reviewerDTO);
		
		return result;
	}
	
	@DeleteMapping("/reviewer/delete/{id}")
	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id")Long Id){
		HashMap<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = reviewerRepository.findById(Id)
				.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", Id));
		ReviewerDTO reviewerDTO = modelMapper.map(reviewerEntity, ReviewerDTO.class);
		reviewerRepository.delete(reviewerEntity);
	
		result.put("Status", 200);
		result.put("Message", "Delete Reviewer Success");
		result.put("Data", reviewerDTO);
		
		return result;
	}
}
