package com.example.Movie.dto;

import java.util.Date;

public class MovieDTO {
	private long id;
	private Date dateRelease;
	private String language;
	private String releaseCountry;
	private int time;
	private String title;
	private int year;
	
	public MovieDTO() {
		super();
	}

	public MovieDTO(long id, Date dateRelease, String language, String releaseCountry, int time, String title,
			int year) {
		super();
		this.id = id;
		this.dateRelease = dateRelease;
		this.language = language;
		this.releaseCountry = releaseCountry;
		this.time = time;
		this.title = title;
		this.year = year;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDateRelease() {
		return dateRelease;
	}

	public void setDateRelease(Date dateRelease) {
		this.dateRelease = dateRelease;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getReleaseCountry() {
		return releaseCountry;
	}

	public void setReleaseCountry(String releaseCountry) {
		this.releaseCountry = releaseCountry;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	
}
