package com.example.Movie.dto;

public class ReviewerDTO {
	private long id;
	private String reviewerName;
	
	public ReviewerDTO() {
		super();
	}

	public ReviewerDTO(long id, String reviewerName) {
		super();
		this.id = id;
		this.reviewerName = reviewerName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}
	
}
