package com.example.Movie.dto;

import com.example.Movie.models.Movie;
import com.example.Movie.models.RatingId;
import com.example.Movie.models.Reviewer;

public class RatingDTO {
	private RatingId id;
	private Movie movie;
	private Reviewer reviewer;
	private int numOfRatings;
	private int reviewerStars;
	
	public RatingDTO() {
		super();
	}

	public RatingDTO(RatingId id, Movie movie, Reviewer reviewer, int numOfRatings, int reviewerStars) {
		super();
		this.id = id;
		this.movie = movie;
		this.reviewer = reviewer;
		this.numOfRatings = numOfRatings;
		this.reviewerStars = reviewerStars;
	}

	public RatingId getId() {
		return id;
	}

	public void setId(RatingId id) {
		this.id = id;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public int getNumOfRatings() {
		return numOfRatings;
	}

	public void setNumOfRatings(int numOfRatings) {
		this.numOfRatings = numOfRatings;
	}

	public int getReviewerStars() {
		return reviewerStars;
	}

	public void setReviewerStars(int reviewerStars) {
		this.reviewerStars = reviewerStars;
	}
	
	
}
